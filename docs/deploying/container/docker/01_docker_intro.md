# Docker Introduction

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [What Services Docker Offered](#what-services-docker-offered)
2. [Docker Architecture](#docker-architecture)

<!-- /code_chunk_output -->

## What Services Docker Offered

1. `repository` ==> server ==> application publishing
1. `image` ==> file compression ==> application deliver format
1. `container engine` ==> light-weight virtualization ==> application runtime
1. `container` ==> process ==> application runtime
1. `container orchestration` ==> resource manager ==> application lifecycle management

## Docker Architecture

![docker_engine-components-flow](./assets_image/docker_engine-components-flow.png)

>original: <https://docs.docker.com/get-started/overview/>

![docker_architecture](./assets_image/docker_architecture.svg)

>original: <https://docs.docker.com/get-started/overview/>
