# 构建Build

## 构建是什么

>代码变成可执行文件，叫做编译（compile）；先编译这个，还是先编译那个（即编译的安排），叫做构建（build）。  
>[阮一峰. Make 命令教程.](http://www.ruanyifeng.com/blog/2015/02/make.html)

按句话说，构建是编译的编排（orchestration）。这个编排过程除了编译调度外，还包括环境与依赖`environment & dependencies`、测试`testing`、打包`packaging`、报告`reporting`等，还可能包含目标代码分发`distribution`等。

## IDE与构建

`IDE`适合代码编辑和启动其他工具（包括构建工具）

## 构建的流程

## 主流构建工具

### Make

1. 可能是第一个构建工具：1977年，`Bell Labs`的`Stuart Feldman`开发
1. 与具体的编程语言无关，但主要用于`C/C++`语言
1. 主流的`Unix-Like`和`Windows`都有衍生实现，如：`GNU Make`、`BSD MAKE`、`Windows nmake`
1. 利用`OS`本地命令完成每个构建目标的实际行为

### Java的主流构建工具

1. `Apache Ant`
    + 用于构建`Java`项目
    + 使用`XML`文件格式，解决`makefile`的`space`、`tab`问题
    + 构建脚本与平台无关
    + 命令式/过程式：开发者定义每一个目标以及完成该目标所需要执行的任务
1. `Apache Maven`
    + 解决第三方依赖管理问题，`maven repository`提供大量的`Java`开源软件包
    + 基于约定提供开箱即用的一些构建目标
    + 声明式：开发者定义目标，插件执行该目标所需要执行的任务（提供了大量开箱即用的插件）
1. `Gradle`
    + `SDLC`全生命周期支持：检查、编译、测试、`API`文档生成、打包和部署

## 脚手架Scaffolding

## References and Futhermore

1. [List of build automation software](https://en.wikipedia.org/wiki/List_of_build_automation_software)
1. [阮一峰. 编译器的工作过程](http://www.ruanyifeng.com/blog/2014/11/compiler.html)
1. [Alex Smith. Building C Projects](http://nethack4.org/blog/building-c.html)
