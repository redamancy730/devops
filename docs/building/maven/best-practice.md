# 最佳实践Best Practices

## 约定优于配置Convention Over Configuration

### 目录结构约定

### 命名约定

1. `groupId`：`corporation.project-group-name`
1. `artifactId`: `project-name`
1. `package`: `${groupId}.${artifactId-like}.xxx`
    - `${groupId}`: 建议与`groupId`一致
    - `${artifactId-like}`: 建议包含或体现`artifactId`，如，将`artifactId`去除`-`形成一个包或包层级

多模块命名：

1. 各子模块使用相同的`groupId`
1. 各子模块的`artifactId`使用相同的前缀
1. 各子模块的`dirname`使用`artifactId`（删除前缀并替换连字符为下划线后的`artifactId`）
1. 一起开发和发布的子模块使用相同的`version`
