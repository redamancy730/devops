# Ant

## Ant是什么

`Ant(Another Neat Tool)`，`Ant`可以看成是一个`Java`版的`Make`

## build.xml的核心

1. 基本结构：目标`target`、依赖`depends`、任务`task`，分别对应`Make`的目标`target`、依赖`prerequisite`和命令`command`
1. 开发者定义实现`target`的`task`
1. `Ant`定义了内置的用`Java`实现的`task`（也可以通过`exec`执行本地命令）
1. 外接依赖管理：`Ant`没有内置的依赖管理功能（可以通过`Ivy`进行依赖管理）
