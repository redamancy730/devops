---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# PlantUML Intro

<!-- slide -->
# What is PlantUML

>PlantUML is an **open-source tool** allowing users to create **UML diagrams** from a **plain text language**. The language of PlantUML is an example of a **Domain-Specific Language**. It uses **Graphviz** software to **lay out its diagrams**. It has been used to allow blind students to work with UML. PlantUML also helps blind software engineers to design and read UML diagrams.

<!-- slide -->
# What is Domain-Specific Language

>A domain-specific language (DSL) is a **computer language specialized to a particular application domain**. This is in contrast to a **general-purpose language (GPL), which is broadly applicable across domains**.

<!-- slide -->
# What UML Diagrams PlantUML Supported

1. :white_check_mark: **Sequence diagram时序图/顺序图**
1. :white_check_mark: **Use Case diagram用例图**
1. :white_check_mark: **Class diagram类图**
1. :white_check_mark: **Activity diagram活动图**
1. :white_check_mark: **Component diagram组件图**
1. :white_check_mark: **State diagram状态图**
1. Object diagram对象图
1. :white_check_mark: **Deployment diagram部署图**
1. Timing diagram定时图

<!-- slide -->
# What non-UML Diagrams PlantUML Supported

1. :white_check_mark: **Wireframe graphical interface线框图形界面（低保真实物原型）**
1. :white_check_mark: **Archimate diagram架构图**
1. :white_check_mark: **Specification and Description Language (SDL)规范与描述语言**
1. Ditaa diagram
1. Gantt diagram甘特图
1. :white_check_mark: **MindMap diagram脑图/思维导图**
1. :white_check_mark: **Work Breakdown Structure diagram工作分解图**
1. :white_check_mark: **Mathematic with AsciiMath or JLaTeXMath notation数学公式**
1. :white_check_mark: **Entity Relationship diagram实体-关系图**

<!-- slide -->
# Famous Applications that use PlantUML

1. ATOM, VSCode
1. Eclipse, IntelliJ IDEA, NetBeans
1. Vim
1. LaTeX
1. LibreOffice
1. MS Word 2010 on Windows (32/64 bit) : [`Word Add-in`](https://plantuml.com/word)

<!-- slide -->
# Thank You

## :ok:End of This Slide:ok:
