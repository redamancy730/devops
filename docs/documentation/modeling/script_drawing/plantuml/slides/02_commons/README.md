---
presentation:
    theme: white.css
    width: 1200
    height: 800
    slideNumber: true
    enableSpeakerNotes: true
    transition: zoom
    transitionSpeed: default
---

<!-- slide -->
# Commons in All Diagrams

<!-- slide -->

# Commons

1. **代码区域：**  **`@startuml [diagram name]`** 开始、**`@enduml`** 结束
1. **隐式图形类型：** 不必显示声明具体图示类型（一个图示可以包含多种图示）
1. **注释：** 支持单行注释和多行注释
    + 单行注释：**`'`**
    + 多行注释：**`/'`** 和 **`'/`**
1. **转义字符：** 反斜杠 **`\`** 转义保留字符，**`\n`** 经常被用于文本换行
1. **`Creole`标记（支持部分HTML标签）：** 文本内容支持 **`Creole`** 标记（与 **Markdown** 基本类似）
1. **`skinparam`命令：** 用于定义图示的样式（颜色、字体、对齐等）

## :point_right: `skinparam` is short of `"skin parameter"` :point_left:

<!-- slide -->

# Commons Docs

1. [**`Common commands`**](https://plantuml.com/commons): <https://plantuml.com/commons>
1. [**`Skinparam`**](http://plantuml.com/skinparam): <http://plantuml.com/skinparam>
1. [**`Creole Tag`**](http://plantuml.com/creole): <http://plantuml.com/creole>
1. [**`Colors`**](https://plantuml.com/color): <https://plantuml.com/color>
1. [**`Preprocessing`**](https://plantuml.com/preprocessing): <https://plantuml.com/preprocessing>

<!-- slide -->
# Title

+ **`title`** ：定义单行标题
+ **`title`** 和 **`end title`** ： 定义多行标题

<!-- slide -->
# Header and Footer

+ **`[right(default)/left/center] header`** ：定义单行页脚
+ **`[right/left/center(default)] footer`** ：定义单行页脚
+ **`[right(default)/left/center] header`** 和 **`endheader`** ：定义多行页眉
+ **`[right/left/center(default)] footer`** 和 **`endfooter`** ：定义多行页脚

<!-- slide -->
# Caption and Legend

1. **`caption`** 定义图示的题注，只支持单行字符串（但可以用 **`\n`** 换行）
1. **`legend <bottom(default)/top> <center(default)/left/right> - end legend`** 定义图示的图例

<!-- slide -->
# Commons: Note

```{.line-numbers}
'single line note
note <left/right/top/bottom> of <object id> : <single-line text>

'multi lines note
note <left/right/top/bottom> of <object id>
<multi-lines text>
end note
```

<!-- slide -->

@import "../../resources/sample/commons_note.puml"

<!-- slide -->
# Commons: Zoom

1. **`scale`** 关键字
1. 数值可以是：
    1. 倍数： 整数、小数、分数
    1. 像素数（可选 **`max`** 关键字）： 长（像素）、宽（像素）、长×宽（像素）

<!-- slide -->
# Skinparam

1. 在图示文件中直接定义
1. **`!include`** 预处理指令
1. 命令行的配置文件

<!-- slide -->
# Skinparam Syntax

```{.line-numbers}
'method 01
skinparam xxxxParam1 value1
skinparam xxxxParam2 value2
skinparam xxxxParam3 value3

'method 02
skinparam xxxx {
    Param1 value1
    Param2 value2
    Param3 value3
}
```

<!-- slide -->
# Skinparam FontColor, FontSize, FontName

1. **`xxxFontColor`**, **`xxxFontSize`**, **`xxxFontName`**, **`xxx`** is the element-name(eg: class)
1. **`defaultFontColor`**, **`defaultFontSize`**, **`defaultFontName`**

<!-- slide -->

@import "../../resources/sample/commons.puml"

<!-- slide -->
# Thank You

## :ok:End of This Slide:ok:
