# Some Details

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [`git merge` vs `git rebase`](#git-merge-vs-git-rebase)
2. [`git push --force` vs `git push --force-with-lease`](#git-push-force-vs-git-push-force-with-lease)
3. [`git checkout` vs `git reset` vs `git restore`](#git-checkout-vs-git-reset-vs-git-restore)
4. [Bibliographies](#bibliographies)

<!-- /code_chunk_output -->

## `git merge` vs `git rebase`

## `git push --force` vs `git push --force-with-lease`

## `git checkout` vs `git reset` vs `git restore`

>1. [atlassian. Resetting, Checking Out & Reverting.](https://www.atlassian.com/git/tutorials/resetting-checking-out-and-reverting)
>1. [stephencharlesweiss. git restore vs reset vs revert: the basics.](https://stephencharlesweiss.com/git-restore-reset-revert)

## Futhermore
