# Extra-Tools

<!-- @import " [TOC] " {cmd="toc" depthFrom=2 depthTo=6 orderedList=true} -->

<!-- code_chunk_output -->

1. [👍 `git-toolbelt` by `Vincent Driessen`](#git-toolbelt-by-vincent-driessen)
2. [`git-extras`](#git-extras)

<!-- /code_chunk_output -->

## 👍 `git-toolbelt` by `Vincent Driessen`

1. [nvie/git-toolbelt](https://github.com/nvie/git-toolbelt)
1. [Vincent Driessen. Git power tools for daily use.](https://nvie.com/posts/git-power-tools/)

## `git-extras`

1. [tj/git-extras](https://github.com/tj/git-extras)
