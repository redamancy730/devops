# Issue

## What is Issue

### What Issue Can Do

>Issues can be bugs, tasks or ideas to be discussed. Also, issues are searchable and filterable.
>
>gitlab.com

## CASE Project Management Tools

### Microsoft Project

### Tencent TAPD

>homepage: <https://www.tapd.cn/>

## Bibliography

1. 如何使用 Issue 管理软件项目？: <http://www.ruanyifeng.com/blog/2017/08/issue.html>
1. <https://robinpowered.com/blog/best-practice-system-for-organizing-and-tagging-github-issues/>

## Furthermore
