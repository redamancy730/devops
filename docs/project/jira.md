# `jira`

## What

`jira`最早用于`bug tracking`，`jira`的核心结构是`project-issue-board`，在`jira`，`agile`的`epic`、`feature`、`story`、`task`都是`issue`的类型（`epic - story - task`三层结构，没有明确区别`feature`和`story`）。

### Family of `jira` Products

1. `Jira Software` is built specifically for software teams. Jira Software combines powerful developer tool integrations with the most important features and functionality required for great agile software development.
1. `Jira Service Management` is built for IT and service teams, providing them with everything they need for out-of-the-box incident, problem, and change management.
1. `Jira Core`/`jira work management` is a simplified project management tool for customers looking to extend the power of Jira to their organization.

### Key Terms

1. `project`: a project is a collection of `issues`, `projects` can be organized by teams or larger deliverables
1. `issue`: an `issue` an individual work item
1. `issue card`: `issue card` is a look-like when issue is shown on a `board`
1. `board`: a `board` is a visual display of working progress (`sprint` when uses `scrum`), often with 3-4 columns
1. `kanban board`: `kanban boards` show a continuous flow of work, `issues` come in and out of the board from start to finish.
1. `scrum board`: `scrum boards` bring in groups of issues that are worked on during a fixed period of work time, often a two-week "sprint."
1. `status`: a `status` shows the current progress of an issue. Common statuses are "To Do, In Progress, In Review, Done."
1. `workflow`: a `workflow` is the path of `statuses` an `issue` will go through from start to finish
1. `agile`: a project management methodology. It takes an iterative approach to managing projects, focusing on continuous release that require regular updates based on feedback. Agile helps teams increase delivery speed, expand collaboration, and better respond to market trends
1. `scrum`: a specific agile project management framework to help design, develop, and deliver complex products. It's mostly used by software development teams. Scrum teams organize their work in sprints. A sprint is a short, fixed period when a scrum team works to complete a batch of issues

#### Team-managed projects VS Company-managed projects

## How

### Plans and Pricing

>1. [plans and pricing](https://www.atlassian.com/software/jira/pricing)

#### Free vs Standard

||free|standard|
|:--|:--:|:--:|
|user limit|10|20,000|
|project roles|-|☑️|
|advanced permissions|-|☑️|
|audit logs|-|☑️|
|anonymous access|-|☑️|
|data residency|-|☑️|
|file storage|2GB|250GB|

❗ 免费版所有成员都是`administrator`权限 ❗

### Client

>Sunsetting the Jira Cloud for Mac app. Jira Cloud for web & mobile continues to accelerate.
>
>>[Announcement: Sunsetting the Jira Cloud for Mac App](https://community.atlassian.com/t5/Jira-Mobile-Apps-articles/Announcement-Sunsetting-the-Jira-Cloud-for-Mac-App/ba-p/1911778)



### Roles

从`jira`作为软件的角度看，`jira`有两种角色：

1. `administrator`
1. `user`

### Issues

`issues type`和`issues hierarchy`是关键。

>1. [atlassian. Create an issue and a sub-task.](https://support.atlassian.com/jira-software-cloud/docs/create-an-issue-and-a-sub-task/)
>1. [What's Epic, User Story and Task in Scrum Work Hierarchy](https://adaptmethodology.com/epic-user-story-task/)

## Futhermore

### Tutorials

1. [Learn Jira with Atlassian University](https://university.atlassian.com/student/path/871316-learn-jira-with-atlassian-university?utm_source=jira-help&utm_medium=inapp&utm_campaign=P:uni-training*O:university*I:in-app-help*)
    1. 👍 👑 [Jira Fundamentals](https://university.atlassian.com/student/collection/850385/path/1083901)
    1. 👍 👑 [Jira Service Management Fundamentals](https://university.atlassian.com/student/collection/850385/path/1277309)

### Documentation

1. [Jira Software Cloud support](https://support.atlassian.com/jira-software-cloud/)
